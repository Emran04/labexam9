<?php
session_start();
require_once '../vendor/autoload.php';

use App\User\User;
use App\Homes\Home;
use App\User\Auth;
use App\Message\Message;

$user = new User();
$home = new Home();
$auth = new Auth();

$status = $auth->prepare($_POST)->isLoggedIn();
if(!$status) {
    Message::message('You Must be logged in to access this page', 'danger');
    header('Location: index.php');
}

if(!empty($_SESSION['message'])) {
    $message = Message::message();
}

$_POST['email'] = $_SESSION['user_email'];

$sUser  = $user->prepare($_POST)->view();

$homes = $home->prepare($_POST)->view();

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../resources/bootstrap/css/jumbotron-narrow.css" rel="stylesheet">

</head>

<body>

<div class="container">
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills pull-right">
                <li role="presentation" class="active"><a href="index.php">Home</a></li>
                <li role="presentation"><a href="edit.php">Update Profile</a></li>
                <li role="presentation"><a href="auth/logout.php">Log Out</a></li>
            </ul>
        </nav>
        <h3 class="text-muted">Profile</h3>
    </div>
    <?php if(isset($message)) : ?>
        <div id="message" class="alert alert-<?= $message['lvl'];  ?>" role="alert">
            <?php echo $message['message'];  ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-12">
            <p>
                <a href="homes/homecreate.php" class="btn btn-primary">Create Home</a>
            </p>
        </div>
    </div>

    <div class="jumbotron">
        <h1>Welcome <?= $sUser['first_name'] ?></h1>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            User Information
        </div>
        <div class="panel-body">
            <div class="list-group">
                <div class="list-group-item">
                    Name: <?= $sUser['first_name'] ?> <?= $sUser['last_name'] ?>
                </div>
                <div class="list-group-item">
                    Email: <?= $sUser['email'] ?>
                </div>
                <div class="list-group-item">
                    Phone: <?= $sUser['phone'] ?>
                </div>
                <div class="list-group-item">
                    Gender: <?= $sUser['gender'] ?>
                </div>
            </div>
        </div>
    </div>


    <table class="table table-bordered">
        <thead>
        <tr>
            <th>SL</th>
            <th>Title</th>
            <th>Location</th>
            <th>Baths</th>
            <th>Beds</th>
            <th>Price</th>
            <th class="actions">Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl = 0;
        foreach($homes as $sHome):
            $sl++;

            ?>
            <tr>
                <td><?= $sl ?></td>
                <td><?= $sHome['title'] ?></td>
                <td><?= $sHome['home_location'] ?></td>
                <td><?= $sHome['bathrooms'] ?></td>
                <td><?= $sHome['bedrooms'] ?></td>
                <td><?= $sHome['price'] ?></td>
                <td class="actions">
                    <a href="view.php?id=<?= $birthday['id'] ?>" class="btn btn-primary">View</a>
                    <a href="edit.php?id=<?= $birthday['id'] ?>" class="btn btn-primary">Edit</a>
                    <a href="delete.php?id=<?= $birthday['id'] ?>" class="btn btn-danger dl">Delete</a>
                    <a href="trash.php?id=<?= $birthday['id'] ?>" class="btn btn-warning">Trash</a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>



</div> <!-- /container -->

</body>
</html>

