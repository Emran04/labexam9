<?php
session_start();
require_once '../../vendor/autoload.php';

use App\User\User;
use App\User\Auth;
use App\Message\Message;

$user = new User();
$auth = new Auth();

$status = $auth->prepare($_POST)->isLoggedIn();
if(!$status) {
    Message::message('You Must be logged in to access this page', 'danger');
    header('Location: ../index.php');
}


$_POST['email'] = $_SESSION['user_email'];

$sUser  = $user->prepare($_POST)->view();

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create Home</title>

    <!-- Bootstrap core CSS -->
    <link href="../../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../resources/bootstrap/css/jumbotron-narrow.css" rel="stylesheet">

</head>

<body>

<div class="container">
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills pull-right">
                <li role="presentation" class="active"><a href="../index.php">Home</a></li>
                <li role="presentation"><a href="../edit.php">Update Profile</a></li>
                <li role="presentation"><a href="../auth/logout.php">Log Out</a></li>
            </ul>
        </nav>
        <h3 class="text-muted">Profile</h3>
    </div>

    <div class="jumbotron">
        <h1>Welcome <?= $sUser['first_name'] ?></h1>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Enter Your Home details
                    </div>
                    <div class="panel-body">
                        <!--sign up-->
                        <form action="homestore.php" method="post">
                            <input type="hidden" name="user_id" value="<?= $sUser['id'] ?>">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" id="title" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="home_location">Location</label>
                                <input type="text" name="home_location" id="home_location" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="bedrooms">Bedrooms</label>
                                <input type="number" name="bedrooms" id="bedrooms" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="bathrooms">Bathrooms</label>
                                <input type="number" name="bathrooms" id="bathrooms" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="price">Price Per Night</label>
                                <input type="number" name="price" id="price" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Create" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>




</div> <!-- /container -->

</body>
</html>