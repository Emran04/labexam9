<?php
session_start();
require_once '../vendor/autoload.php';

use App\User\User;
use App\User\Auth;
use App\Message\Message;

$user = new User();
$auth = new Auth();

$status = $auth->prepare($_POST)->isLoggedIn();

if(!empty($_SESSION['message'])) {
    $message = Message::message();
}
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Profile</title>

    <!-- Bootstrap core CSS -->
    <link href="../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../resources/bootstrap/css/jumbotron-narrow.css" rel="stylesheet">

</head>

<body>

<div class="container">
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills pull-right">
                <li role="presentation" class="active"><a href="welcome.php">Welcome</a></li>
                <?php if($status) : ?>
                <li role="presentation"><a href="auth/logout.php">Log Out</a></li>
                <?php else: ?>
                <li role="presentation"><a href="index.php">Log in</a></li>
                <?php endif; ?>

            </ul>
        </nav>
        <h3 class="text-muted">Profile</h3>
    </div>
    <?php if(isset($message)) : ?>
        <div id="message" class="alert alert-<?= $message['lvl'];  ?>" role="alert">
            <?php echo $message['message'];  ?>
        </div>
    <?php endif; ?>

    <?php if(!$status): ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Login
                </div>
                <div class="panel-body">
                    <!--Log in-->
                    <form action="auth/login.php" method="post">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Log In" class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Signup
                </div>
                <div class="panel-body">
                    <!--sign up-->
                    <form action="auth/register.php" method="post">
                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" name="first_name" id="first_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="last_name">Last Name</label>
                            <input type="text" name="last_name" id="last_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" id="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" name="phone" id="phone" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email">Gender</label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="gender" value="Male" checked>
                                    Male
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="gender" value="Female">
                                    Female
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Sign up" class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>



</div> <!-- /container -->

</body>
</html>

