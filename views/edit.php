<?php
session_start();
require_once '../vendor/autoload.php';

use App\User\User;
use App\User\Auth;
use App\Message\Message;

$user = new User();
$auth = new Auth();

$status = $auth->prepare($_POST)->isLoggedIn();
if(!$status) {
    Message::message('You Must be logged in to access this page', 'danger');
    header('Location: index.php');
}

if(!empty($_SESSION['message'])) {
    $message = Message::message();
}

$_POST['email'] = $_SESSION['user_email'];

$sUser  = $user->prepare($_POST)->view();

//var_dump($sUser);

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Narrow Jumbotron Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../resources/bootstrap/css/jumbotron-narrow.css" rel="stylesheet">

</head>

<body>

<div class="container">
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills pull-right">
                <li role="presentation" class="active"><a href="index.php">Home</a></li>
                <li role="presentation"><a href="auth/logout.php">Log Out</a></li>
            </ul>
        </nav>
        <h3 class="text-muted">Profile</h3>
    </div>
    <?php if(isset($message)) : ?>
        <div id="message" class="alert alert-<?= $message['lvl'];  ?>" role="alert">
            <?php echo $message['message'];  ?>
        </div>
    <?php endif; ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Update Information
                    </div>
                    <div class="panel-body">
                        <!--sign up-->
                        <form action="auth/update.php" method="post">
                            <input type="hidden" name="id" value="<?= $sUser['id'] ?>">
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" name="first_name" value="<?= $sUser['first_name'] ?>" id="first_name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" value="<?= $sUser['last_name'] ?>" name="last_name" id="last_name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name="email" value="<?= $sUser['email'] ?>" id="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" name="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="text" name="phone" value="<?= $sUser['phone'] ?>" id="phone" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="email">Gender</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="gender" value="Male" <?php if($sUser['gender'] == 'Male') echo 'checked' ?>>
                                        Male
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="gender" value="Female" <?php if($sUser['gender'] == 'Female') echo 'checked' ?>>
                                        Female
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="phone">Birthday</label>
                                <input type="date" name="birthday" value="<?= $sUser['birthday'] ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="location">Location</label>
                                <input type="text" name="location" value="<?= $sUser['location'] ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Update" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div> <!-- /container -->

</body>
</html>

