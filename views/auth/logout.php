<?php
session_start();
require_once '../../vendor/autoload.php';

use App\User\User;
use App\User\Auth;
use App\Message\Message;

$user = new User();
$auth = new Auth();

$status = $auth->logout();
Message::message('You logged out successfully!!', 'success');
header('Location: ../index.php');


