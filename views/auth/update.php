<?php

require_once '../../vendor/autoload.php';

use App\User\User;
use App\User\Auth;

$user = new User();
$auth = new Auth();

$user->prepare($_POST)->update();

