<?php
session_start();
require_once '../../vendor/autoload.php';

use App\User\User;
use App\User\Auth;
use App\Message\Message;

$user = new User();
$auth = new Auth();

$status = $auth->prepare($_POST)->isRegistered();

if($status) {
    $_SESSION['user_email'] = $_POST['email'];
    header('Location: ../welcome.php');
}else {
    Message::message('Your email and password does not match!!!', 'danger');
    header('Location: ../index.php');
}

