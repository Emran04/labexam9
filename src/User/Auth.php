<?php

namespace App\User;

if(!isset($_SESSION)){
    session_start();
}
use App\Message\Message;
use App\DB\Database as DB;
class Auth extends DB
{
    public $id = '',
        $email = '',
        $password = '';

    public function prepare($data = [])
    {
        if(array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if(array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if(array_key_exists('password', $data)) {
            $this->password = md5($data['password']);
        }

        return $this;
    }


    public function isExist()
    {
        $query = "SELECT * FROM `users` WHERE `email` = '{$this->email}'";
        $result = mysqli_query($this->conn, $query);

        if(mysqli_num_rows($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function isRegistered()
    {
        $query = "SELECT * FROM `users` WHERE `email` = '{$this->email}' AND `password` = '{$this->password}'";
        $result = mysqli_query($this->conn, $query);

        if(mysqli_num_rows($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function isLoggedIn()
    {
        if( isset($_SESSION) && !empty($_SESSION['user_email']) ) {
            return true;
        }else {
            return false;
        }
    }

    public function logout()
    {
        unset($_SESSION['user_email']);
    }
}
