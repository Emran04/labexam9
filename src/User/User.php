<?php

namespace App\User;

use App\Message\Message;
use App\DB\Database as DB;
class User extends DB
{
    public $id = '',
            $first_name = '',
            $last_name = '',
            $email = '',
            $phone = '',
            $gender = '',
            $birthday = '',
            $location = '',
            $password = '';

    public function prepare($data = [])
    {
        if(array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if(array_key_exists('first_name', $data)) {
            $this->first_name = $data['first_name'];
        }
        if(array_key_exists('last_name', $data)) {
            $this->last_name = $data['last_name'];
        }
        if(array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if(array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
        }
        if(array_key_exists('gender', $data)) {
            $this->gender = $data['gender'];
        }
        if(array_key_exists('birthday', $data)) {
            $this->birthday = $data['birthday'];
        }
        if(array_key_exists('location', $data)) {
            $this->location = $data['location'];
        }
        if(array_key_exists('password', $data) && !empty($data['password'])) {
            $this->password = md5($data['password']);
        }

        return $this;
    }


    public function store()
    {
        $query = "INSERT INTO `users` (`first_name`, `last_name`, `email`, `phone`, `gender`, `password`) VALUES ('{$this->first_name}', '{$this->last_name}', '{$this->email}', '{$this->phone}', '{$this->gender}', '{$this->password}')";
        $result = mysqli_query($this->conn, $query);

        $id = mysqli_insert_id($this->conn);
        $query = "INSERT INTO `profiles` (`user_id`) VALUES ({$id})";

        $result = mysqli_query($this->conn, $query);
        if($result) {
            Message::message('You registered successfully !!', 'success');
            header('Location: ../index.php');
        }
    }

    public function view()
    {
        $query = "SELECT `users`.`id`, `users`.`first_name`, `users`.`last_name`, `users`.`email`, `users`.`phone`, `users`.`gender`, `profiles`.`birthday`, `profiles`.`location`
                FROM `users`
                INNER JOIN `profiles`
                ON `users`.`id` = `profiles`.`user_id`
                WHERE `users`.`email` = '{$this->email}'";
        $result = mysqli_query($this->conn, $query);

        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    public function update()
    {
        if(!empty($this->password)) {
            $query = "UPDATE `users`
                  INNER JOIN `profiles`
                  ON `users`.`id` = `profiles`.`user_id`
                  SET `users`.`first_name` = '{$this->first_name}',`users`.`last_name` = '{$this->last_name}',`users`.`email` = '{$this->email}', `users`.`phone` = '{$this->phone}', `users`.`gender` = '{$this->gender}', `users`.`password` = '{$this->password}', `profiles`.`birthday` = '{$this->birthday}',  `profiles`.`location` = '{$this->location}'
                  WHERE `users`.`id` = {$this->id}";
        } else {
            $query = "UPDATE `users`
                  INNER JOIN `profiles`
                  ON `users`.`id` = `profiles`.`user_id`
                  SET `users`.`first_name` = '{$this->first_name}',`users`.`last_name` = '{$this->last_name}',`users`.`email` = '{$this->email}', `users`.`phone` = '{$this->phone}', `users`.`gender` = '{$this->gender}', `profiles`.`birthday` = '{$this->birthday}',  `profiles`.`location` = '{$this->location}'
                  WHERE `users`.`id` = {$this->id}";
        }
        $result = mysqli_query($this->conn, $query);

        if($result) {
            Message::message('You profile information successfully updated !!', 'success');
            header('Location: ../welcome.php');
        }

    }

}
