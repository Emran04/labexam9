<?php

namespace App\Homes;

use App\Message\Message;
use App\DB\Database as DB;

class Home extends DB
{
    public $id = '',
        $email = '',
        $user_id = '',
        $title = '',
        $home_location = '',
        $bedrooms = '',
        $bathrooms = '',
        $price = '',
        $order_dates = '';

    public function prepare($data = [])
    {
        if(array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if(array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if(array_key_exists('user_id', $data)) {
            $this->user_id = $data['user_id'];
        }
        if(array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if(array_key_exists('home_location', $data)) {
            $this->home_location = $data['home_location'];
        }
        if(array_key_exists('bedrooms', $data)) {
            $this->bedrooms= $data['bedrooms'];
        }
        if(array_key_exists('bathrooms', $data)) {
            $this->bathrooms = $data['bathrooms'];
        }
        if(array_key_exists('price', $data)) {
            $this->price = $data['price'];
        }
        if(array_key_exists('order_dates', $data)) {
            $this->order_dates = $data['order_dates'];
        }

        return $this;
    }


    public function store()
    {
        $query = "INSERT INTO `homes` (`user_id`, `title`, `home_location`, `bedrooms`, `bathrooms`, `price`) VALUES ('{$this->user_id}', '{$this->title}', '{$this->home_location}', '{$this->bedrooms}', '{$this->bathrooms}', '{$this->price}')";
        $result = mysqli_query($this->conn, $query);
        if($result) {
            Message::message('Your Home created successfully !!', 'success');
            header('Location: ../welcome.php');
        }
    }

    public function view()
    {
        $_allRslts = [];
        $query = "SELECT `users`.`id`, `homes`.`title`, `homes`.`home_location`, `homes`.`bathrooms`, `homes`.`bedrooms`, `homes`.`price`
                FROM `users`
                INNER JOIN `homes`
                ON `users`.`id` = `homes`.`user_id`
                WHERE `users`.`email` = '{$this->email}'";
        $results = mysqli_query($this->conn, $query);

        while($row = mysqli_fetch_assoc($results)) {
            $_allRslts[] = $row;
        }

        return $_allRslts;
    }



}
